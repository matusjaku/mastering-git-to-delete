I like your interactiveness at the seminars - it forces a student to really pay an attention since he/she can be another victim of a pick!

One of the things you've learned me was the story behind git stash command: I've always took it as some special object. However, as you said, internally it's rather implemented as a special branch, since git developers wanted to reuse the existing API above branches they already had.

During the course, I've realized I want to also understand better how git works internally. I'll start to read Git Internals chapter of Pro Git book probably very soon.
Do you have any other recommendations guys?

Didn't really feel like something would miss (except internet connection at the last seminar for a while :D). Keep doing the great job!
